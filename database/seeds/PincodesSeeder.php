
<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PincodesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pincodes')->insert([
            ['id'=>1,'pincode'=>"1212",'city'=>"Dhaka",'state'=>"Khilkhet",'created_at'=>' 2020-02-28','updated_at'=>'2020-02-28 13:59:41'],
            ['id'=>2,'pincode'=>"1213",'city'=>"Dhaka",'state'=>"Khilkhet",'created_at'=>' 2020-02-28','updated_at'=>'2020-02-28 13:59:41'],
            ['id'=>3,'pincode'=>"1214",'city'=>"Dhaka",'state'=>"Khilkhet",'created_at'=>' 2020-02-28','updated_at'=>'2020-02-28 13:59:41'],
            ['id'=>4,'pincode'=>"1215",'city'=>"Dhaka",'state'=>"Khilkhet",'created_at'=>' 2020-02-28','updated_at'=>'2020-02-28 13:59:41'],

        ]);
    }
}
