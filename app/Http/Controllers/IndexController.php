<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use App\Banner;

use Illuminate\Http\Request;


class IndexController extends Controller
{

    public  function index(){


        //ascending order by default
       // $ProductsAll =Product::get();


        //Descending order
       // $ProductsAll =Product::orderBy('id','DESC')->get();


        $ProductsAll = Product::inRandomOrder()->where('status',1)->where('feature_item',1)->paginate(6);

//        // Display Categories and Sub-Categories at Left Sidebar Dynamically
//        $categories = Category::with('categories')->where(['parent_id' => 0])->get();
//        // Display Categories and Sub-Categories at Left Sidebar Dynamically End



        // Get All Categories and Sub Categories
        $categories_menu = "";
        $categories = Category::with('categories')->where(['parent_id' => 0])->get();
        $categories = json_decode(json_encode($categories));

        foreach($categories as $cat){
            $categories_menu .= "
            <div class='panel-heading'>
                <h4 class='panel-title'>
                    <a data-toggle='collapse' data-parent='#accordian' href='#".$cat->id."'>
                        <span class='badge pull-right'><i class='fa fa-plus'></i></span>
                        ".$cat->name."
                    </a>
                </h4>
            </div>
            <div id='".$cat->id."' class='panel-collapse collapse'>
                <div class='panel-body'>
                    <ul>";
            $sub_categories = Category::where(['parent_id' => $cat->id])->get();
            foreach($sub_categories as $sub_cat){
                $categories_menu .= "<li><a href='#'>".$sub_cat->name." </a></li>";
            }
            $categories_menu .= "</ul>
                </div>
            </div>
            ";
        }

        // Get All Categories and Sub Categories End
        $banners = Banner::where('status','1')->get();


        // Meta tags
        $meta_title = "E-shop Sample Website";
        $meta_description = "Online Shopping Site for Men, Women and Kids Clothing";
        $meta_keywords = "eshop website, online shopping, men clothing";


        return view('index')->with(compact('ProductsAll','categories_menu','categories','banners','meta_title','meta_description','meta_keywords'));

    }










}
